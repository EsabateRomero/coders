package api.dto;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.sun.istack.NotNull;

@Entity
@Table(name="usuario")
public class Usuario implements Serializable{

	private static final long serialVersionUID = 1490065296337050952L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(unique=true)
	@NotNull
	private String email;
	@NotNull
	private String password;
	
	//hay un metodo que n me acuerdo como se llama pero que nos proporciona de forma automatica la fecha en la que se crea dicho objeto
	//private Date fechaCreacion;
	private String token;
	private boolean isActive;
	
	@NotNull
	private String nombre;
	@NotNull
	private String apellido1;
	@NotNull
	private String apellido2;
	@NotNull
	private Date nacimiento;
	
	
	@Embedded
	private Direccion direccion;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="usuario")
	private List<Posts> posts;

	public Usuario(String email, String password, Date fechaCreacion, String token, boolean isActive, String nombre,
			String apellido1, String apellido2, Date nacimiento, Direccion direccion, List<Posts> posts) {
		super();
		this.email = email;
		this.password = password;
		this.fechaCreacion = fechaCreacion;
		this.token = token;
		this.isActive = isActive;
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.nacimiento = nacimiento;
		this.direccion = direccion;
		this.posts = posts;
	}

	public Usuario(String email, String password, String nombre, String apellido1, String apellido2, Date nacimiento,
			Direccion direccion) {
		super();
		this.email = email;
		this.password = password;
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.nacimiento = nacimiento;
		this.direccion = direccion;
	}
	
	public Usuario() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido1() {
		return apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public Date getNacimiento() {
		return nacimiento;
	}

	public void setNacimiento(Date nacimiento) {
		this.nacimiento = nacimiento;
	}

	public Direccion getDireccion() {
		return direccion;
	}

	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}

	public List<Posts> getPosts() {
		return posts;
	}

	public void setPosts(List<Posts> posts) {
		this.posts = posts;
	}
	
	
	

}
