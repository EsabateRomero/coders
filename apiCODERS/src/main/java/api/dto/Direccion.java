package api.dto;

import javax.persistence.Embeddable;


@Embeddable
public class Direccion{
	private String ciudad;
	private String pais;
	private int codiPostal;
	
	public Direccion() {
		
	}

	public Direccion(String ciudad, String pais, int codiPostal) {
		this.ciudad = ciudad;
		this.pais = pais;
		this.codiPostal = codiPostal;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public int getCodiPostal() {
		return codiPostal;
	}

	public void setCodiPostal(int codiPostal) {
		this.codiPostal = codiPostal;
	}


	
	
	
	
}
