package api.dao;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import api.dto.Usuario;

public interface IUserDao extends JpaRepository<Usuario, Long>{
	public Optional<Usuario> buscarPorCorreo(String email);
}
