package api.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import api.dto.Posts;

public interface IPostsDao extends JpaRepository<Posts, Long>{
	
}
