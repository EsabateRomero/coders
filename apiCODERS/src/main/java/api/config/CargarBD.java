package api.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import api.dao.IUserDao;


@Configuration
public class CargarBD {

	private static final Logger log = LoggerFactory.getLogger(CargarBD.class);
	
	@Bean
	CommandLineRunner initDB(IUserDao iUserDao) {
		return args -> {
			log.info("Cargando " + iUserDao.gu);
		}
	}
	
}
