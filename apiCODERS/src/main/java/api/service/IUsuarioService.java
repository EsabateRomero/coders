package api.service;

import java.util.List;

import api.dto.Usuario;

public interface IUsuarioService {     
	
	public List<Usuario> mostrarAlumnos();
	
	public Usuario guardarUsuario(Usuario usuario);
	
	public Usuario usuarioXemail(String email);
	
	
	
}
