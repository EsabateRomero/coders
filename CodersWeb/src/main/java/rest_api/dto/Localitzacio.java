package rest_api.dto;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Localitzacio {

	@Column(length = 20)
	private String pais;
	
	@Column(length = 30)
	private String comunidadAutonoma;
	
	@Column(length = 30)
	private String provincia;
	
	@Column(length = 30)
	private String ciudad;


	public Localitzacio() {
	
	}

	public Localitzacio(String pais, String comunidadAutonoma, String provincia, String ciudad) {
	
		this.pais = pais;
		this.comunidadAutonoma = comunidadAutonoma;
		this.provincia = provincia;
		this.ciudad = ciudad;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	
	public String getComunidadAutonoma() {
		return comunidadAutonoma;
	}

	public void setComunidadAutonoma(String comunidadAutonoma) {
		this.comunidadAutonoma = comunidadAutonoma;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	
	@Override
	public String toString() {
		return "Localitzacio [pais=" + pais + ", comunidadAutonoma=" + comunidadAutonoma + ", provincia=" + provincia
				+ ", ciudad=" + ciudad + "]";
	}

	
	
}
