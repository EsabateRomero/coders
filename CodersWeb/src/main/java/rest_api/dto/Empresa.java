package rest_api.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;



@Entity
@Table(name = "empresa")
public class Empresa implements Serializable{

	private static final long serialVersionUID = 2416853040501035206L;


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne(mappedBy = "empresa")
	private Empresari empresari;
	
	@Column(nullable = false)
	private String nombre;
	
	private String direccioWeb;
	
	@Embedded
	private Localitzacio localitzacio;
	
	public Empresa() {
		
	}

	public Empresa(Long id, String nombre, String direccioWeb, Localitzacio localitzacio) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.direccioWeb = direccioWeb;
		this.localitzacio = localitzacio;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccioWeb() {
		return direccioWeb;
	}

	public void setDireccioWeb(String direccioWeb) {
		this.direccioWeb = direccioWeb;
	}

	public Localitzacio getLocalitzacio() {
		return localitzacio;
	}

	public void setLocalitzacio(Localitzacio localitzacio) {
		this.localitzacio = localitzacio;
	}
	
	
	
	
}
