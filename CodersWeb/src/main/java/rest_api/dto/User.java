package rest_api.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;

@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "usuarios")
@Entity
public class User implements Serializable{
	

		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		private Long id;
		
		@Embedded
		private Nombre nombre;
		
		
		@Column(name = "nick")
		private String nick;
		@Column(name = "email")
		private String email;
		@Column(name = "password")
		private String password;
		
		@Embedded
		private Localitzacio localitzacio;
		
		private boolean enabled;
		private boolean tokenExpired;

		@Temporal(TemporalType.TIMESTAMP)
		@Column(name = "creado", nullable = false)
		@CreatedDate
		private Date creacioConta;
		public User() {
			
		}

		public User(Nombre nombre, String nick, String email, String password) {
			this.nombre = nombre;
			this.nick = nick;
			this.email = email;
			this.password = password;
		}

		public Long getId() {
			return id;
		}

		
		public String getEmail() {
			return email;
		}

		public String getNick() {
			return nick;
		}

		public boolean isEnabled() {
			return enabled;
		}

		public void setEnabled(boolean enabled) {
			this.enabled = enabled;
		}

		public boolean isTokenExpired() {
			return tokenExpired;
		}

		public void setTokenExpired(boolean tokenExpired) {
			this.tokenExpired = tokenExpired;
		}

		public void setNick(String nick) {
			this.nick = nick;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}
		
		

		public Nombre getNombre() {
			return nombre;
		}

		public void setNombre(Nombre nombre) {
			this.nombre = nombre;
		}

		

		@Override
		public String toString() {
			return "User [id=" + id + ", nombre=" + nombre + ", nick=" + nick + ", email=" + email + ", password="
					+ password + "]";
		}

	

		/*@Override
		public boolean equals(Object obj) {
			if(this==obj)
				return true;SS
			if(obj==null)
				return false;
			if(getClass()!=obj.getClass())
				return false;
			
			User otra = (User) obj;
			if(id==null) {
				if(otra.id!=null)
					return false;
			} else if (!id.equals(otra.id))
				return false;
			return true;
			}*/
		
		
		
}
