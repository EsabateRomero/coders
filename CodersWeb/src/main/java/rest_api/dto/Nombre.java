package rest_api.dto;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.sun.istack.NotNull;

@Embeddable
public class Nombre {
	
	@NotNull
	@Column(length = 20, nullable = false)
	private String nombre;
	
	@Column(length = 40, nullable = false)
	private String cognom1;
	
	@Column(length = 40, nullable = false)
	private String cognom2;
	
	public Nombre() {
		
	}

	public Nombre(String nombre, String cognom1, String cognom2) {
		this.nombre = nombre;
		this.cognom1 = cognom1;
		this.cognom2 = cognom2;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCognom1() {
		return cognom1;
	}

	public void setCognom1(String cognom1) {
		this.cognom1 = cognom1;
	}

	public String getCognom2() {
		return cognom2;
	}

	public void setCognom2(String cognom2) {
		this.cognom2 = cognom2;
	}
	
	
	
	
	
	
}
