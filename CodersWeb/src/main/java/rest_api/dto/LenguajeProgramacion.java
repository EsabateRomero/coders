package rest_api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class LenguajeProgramacion implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5488700315519491063L;
	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nom;
	private Integer nivell;
	
	@ManyToMany(cascade = CascadeType.ALL)
	
	@JoinTable(
			name = "LlenguatgesBuscats",
			joinColumns = @JoinColumn(name="LlenguatgeID", nullable = false),
			inverseJoinColumns = @JoinColumn(name="EmpresarioID", nullable = false)
	
	)	
	
	@JoinColumn(name="empresario", nullable=false)
	private List<Empresari> empresarios;
	
	
	@ManyToMany(cascade = CascadeType.ALL)
	
	@JoinTable(
			name = "LlenguatgesOferits",
			joinColumns = @JoinColumn(name="LlenguatgeID", nullable = false),
			inverseJoinColumns = @JoinColumn(name="ProgramadorID", nullable = false)
	
	)
	@JoinColumn(name="programador", nullable=false)
	private List<Programador> programadores;
	
	
	public LenguajeProgramacion() {
		
	}
	
	public LenguajeProgramacion(Long id, String nom, Integer nivell) {
		this.id = id;
		this.nom = nom;
		this.nivell = nivell;
	}
	
	public void afegirEmpresari(Empresari empresari) {
		if(this.empresarios == null)
			this.empresarios= new ArrayList<>();
		this.empresarios.add(empresari);
	}
	
	public void afegirProgramador(Programador programador) {
		if(this.programadores == null)
			this.programadores= new ArrayList<>();
		this.programadores.add(programador);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Integer getNivell() {
		return nivell;
	}

	public void setNivell(Integer nivell) {
		this.nivell = nivell;
	}
	
	
	
	
	
}
