package rest_api.dto;


import java.io.Serializable;
import java.util.List;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;


@Entity
@DiscriminatorColumn(name = "empresari")
public class Empresari extends User implements Serializable{

	@OneToOne
	@JoinColumn(name = "empresa")
	private Empresa empresa;
	
	@ManyToMany(mappedBy = "empresarios")
	private List<LenguajeProgramacion> lenguajes;
	
	private static final long serialVersionUID = 4951755408694138783L;

	public Empresari() {
		super();
	}

	/*public Empresari(Empresa empresa, List<LenguajeProgramacion> lenguajesDeseados) {
		super();
		this.empresa = empresa;
		this.lenguajesDeseados = lenguajesDeseados;
	}*/

	/*public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public List<LenguajeProgramacion> getLenguajesDeseados() {
		return lenguajesDeseados;
	}

	public void setLenguajesDeseados(List<LenguajeProgramacion> lenguajesDeseados) {
		this.lenguajesDeseados = lenguajesDeseados;
	}*/
	
	
		
	
}
