package rest_api.dao;

import org.springframework.data.repository.CrudRepository;

public interface Empresari extends CrudRepository<Empresari, Long>{

}
