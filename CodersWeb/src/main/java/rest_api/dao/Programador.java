package rest_api.dao;

import org.springframework.data.repository.CrudRepository;

public interface Programador extends CrudRepository<Programador, Long>{

}
