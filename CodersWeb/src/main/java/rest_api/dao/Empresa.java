package rest_api.dao;

import org.springframework.data.repository.CrudRepository;

public interface Empresa extends CrudRepository<Empresa, Long>{

}
