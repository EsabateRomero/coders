package rest_api.dao;

import org.springframework.data.repository.CrudRepository;

public interface LenguajeProgramacion extends CrudRepository<LenguajeProgramacion, Long>{

}
