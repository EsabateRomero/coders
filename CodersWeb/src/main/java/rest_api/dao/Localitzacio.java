package rest_api.dao;

import org.springframework.data.repository.CrudRepository;

public interface Localitzacio extends CrudRepository<Localitzacio, Long>{

}
