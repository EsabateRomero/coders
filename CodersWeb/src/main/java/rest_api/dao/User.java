package rest_api.dao;

import org.springframework.data.repository.CrudRepository;

public interface User extends CrudRepository<User, Long>{

}
